from os import chdir
from os import listdir
from os.path import isfile, join
import re
import nltk.data
import numpy as np
import sqlite3
import wapiti as wp

global tokenizer
global regex_filter
global min_sentence_length
global max_sentence_length

tokenizer = nltk.data.load('tokenizers/punkt/PY3/french.pickle')

accentedLowerCharacters = "àèìòùáéíóúýâêîôûãñõäëïöüÿçøåæœ"
accentedUpperCharacters = "ÁÀÈÎŒÉÍÓÚÝÂÊÎÔÛÃÑÕÄËÏÖÜŸÇßØÅÆ"
regex_filter = re.compile('^[A-Z'+accentedUpperCharacters+'][a-z'+accentedLowerCharacters+']([a-z'+accentedLowerCharacters+accentedUpperCharacters+']*[\s\.?!:;,])+$')
min_sentence_length = 5
max_sentence_length = 20
dbName = 'NLPAutomaticTeaching.db'
dataPath = './Data/'

def product(s):

	product = 1.

	array = s.split()
	for nb in array:
		product *= float(nb)

	return product

def parse_file(path, mod):

	global tokenizer

	piece = ''
	author = ''
	wapitiModel = wp.Model(patterns='*\nU:Wrd X=%x[0,0]', \
		model=mod, \
		lblpost=True, outsc=True)
	sentences = []
	m = re.search('^.*\/(.*?)$', mod).group(1)
	f = open(path, 'r')

	original = f.read()
		
	tokens = list(tokenizer.tokenize(original))
	regex = re.compile('(The Project Gutenberg (E|e)Book( of)?|Project Gutenberg\'s),? ([A-zÀ-ÿ -\'.]+).*?by \n?([A-zÀ-ÿ -]+)', re.DOTALL)
	regex2 = re.compile('^\s*([^\n]*)\n*([A-zÀ-ÿ -]+),? ?([A-zÀ-ÿ -]*)\n.*?\* \* \*', re.DOTALL)
	top = re.search(regex, tokens[0])
	if(top != None):
		(piece, author) = top.group(4, 5)
		#(piece, author) = top.group(1, 2)
		#if(top.group(3) != ''):
		#	author = top.group(3) + ' ' + author
	piece = re.sub('\\n', ' ', piece)
	author = re.sub('\\n', ' ', author)
	print('============================================')
	print(piece, '"'+author+'"')
	print('total sentences: ', len(tokens))
	tokens = filter(tokens)
	print('processed sentences: ', len(tokens))
	for token in tokens:
		info = label_seq(token, wapitiModel)
		processed = re.sub('\\n', ' ', format(token))
		sentences.append(( \
			author, \
			piece, \
			m, \
			token, \
			processed, \
			info[0], \
			info[1], \
			product(info[1]), \
			len(processed.split())
			))
	return sentences

def label_seq(s, model):
	s = format(s)
	info = str(model.label_sequence(s)).split('*')
	info[0] = re.sub(r'.\'', '', re.sub('\\\\n',' ', info[0]))
	info[1] = re.sub('\'', '', info[1])
	
	return info

def filter(tokens):

	global regex_filter

	i = 0
	while(i<len(tokens)):
		if(None == regex_filter.match(tokens[i]) \
			or len(tokens[i].split())>max_sentence_length \
			or len(tokens[i].split())<min_sentence_length):
			tokens.pop(i)
		else:
			tokens[i] = re.sub(r'[\n]',' ', tokens[i])
			i = i+1

	return tokens

def format(s):

	s = re.sub(r'([.|,|?|!|:|;|\'])', r' \1', s)
	words = [w.lower() for w in nltk.word_tokenize(s)]
	words = '\n'.join(w for w in words)

	return words

def addToDatabase(db, path, mod):
	conn = sqlite3.connect(db)
	c = conn.cursor()

	tokens = parse_file(path, mod)
	c.executemany('INSERT OR IGNORE INTO sentences VALUES \
		(?,?,?,?,?,?,?,?,?)', tokens
	)

	conn.commit()
	conn.close()
		
	
######################################################################

conn = sqlite3.connect(dbName)
c = conn.cursor()

# Create table
c.execute('''CREATE TABLE IF NOT EXISTS sentences \
		(author, piece, model, sentence, processed, pos, pos_prob, \
		sentence_prob, size, PRIMARY KEY(model, sentence))''' \
	)
conn.commit()
conn.close()

# For all files in data, add sentences to database
data = [f for f in listdir(dataPath) if isfile(join(dataPath, f))]

for text in data:
	addToDatabase(dbName, dataPath+text, './Models/free-french-treebank/free-french-treebankv2.save')
	addToDatabase(dbName, dataPath+text, './Models/sequoia-corpus-v6.0/sequoia-corpus-v6.0.save')

