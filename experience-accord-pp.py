import sqlite3
import re

avoir = "av(ais|ait|ions|iez|aient)|eu(s|t|rent)|eû(mes|tes)|\
au(rai|ras|ra|rons|rez|ront)|aur(ais|ait|ions|iez|aient)|ai(e|es|t|yons|yez|ent)|eu(sse|sses|ssions|ssiez|ssent)|eût"
dbName = 'NLPAutomaticTeaching.db'

conn = sqlite3.connect(dbName)
c = conn.cursor()

c.execute('''select p.sentence, p.processed, p.pos, p.sentence_prob, p.size from sentences p where p.model = 'sequoia-corpus-v6.0.save';''')

tab = c.fetchall()


c.execute('''select p.sentence, p.processed, p.pos, p.sentence_prob, p.size from sentences p where p.model = 'free-french-treebankv2.save';''')

tab2 = c.fetchall()

i = 0
process = ""
f = open('experience-accord-pp.csv', 'w')

print('==================================================')
for x,z in zip(tab, tab2):
	if(re.search(r'^ET | ET ', x[2]) == None and \
		re.search(r' VPP\s', x[2]) != None and re.search('(^| ?)'+avoir+' ' , x[1]) != None):
		index = -1
		words = x[1].split()
		words2 = z[1].split()
		pos = x[2].split()
		pos2 = z[2].split()
		for y in range(len(words)):
			if(re.search(r"^VPP$", pos[y]) != None):
				index = y
		#index = words.index('tout')
		if(index > -1 and float(x[3])>0 and float(z[3])>0.5 and int(x[4]) > 10 and int(z[4]) > 10 and re.search(r'(ée|ées|és)$', words[index]) != None):
			print(x[0], z[0])
			process += x[0] + '\t' + words[index] + '\t' + words2[index] + '\t' + str(0) + '\n'
			i += 1


process += 'CONDITIONS: SENTENCE PROB > 0.5, SIZE > 10\n'
process += 'Number of sentence: ' + str(i) + '\n'
f.write(process)
f
print(len(tab))
print(i)


