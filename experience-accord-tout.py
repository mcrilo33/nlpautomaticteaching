import sqlite3
import re

dbName = 'NLPAutomaticTeaching.db'

conn = sqlite3.connect(dbName)
c = conn.cursor()

c.execute('''select p.sentence, p.processed, p.pos, p.sentence_prob, p.size from sentences p where p.model = 'sequoia-corpus-v6.0.save';''')

tab = c.fetchall()


c.execute('''select p.sentence, p.processed, p.pos, p.sentence_prob, p.size from sentences p where p.model = 'free-french-treebankv2.save';''')

tab2 = c.fetchall()

i = 0
process = ""
f = open('experience-accord-tout.csv', 'w')

print('==================================================')
for x,z in zip(tab, tab2):
	if(re.search(r'^ET | ET ', x[2]) == None and \
		re.search(r' tou(t|s|ts|es)', x[1]) != None and re.search(r'en tout cas', x[1]) == None):
		index = -1
		words = x[1].split()
		words2 = z[1].split()
		pos = x[2].split()
		pos2 = z[2].split()
		for y in range(len(words)):
			if(re.search(r"^tou(t|s|ts|es)$", words[y]) != None):
				index = y
		#index = words.index('tout')
		if(index > -1 and float(x[3])>0.4 and float(z[3])>0.4 and int(x[4]) > 10 and int(z[4]) > 10):
			#print(x[0], z[0])
			process += x[0] + '\t' + pos[index] + '\t' + pos2[index] + '\t' + str(0) + '\n'
			i += 1


process += 'CONDITIONS: SENTENCE PROB > 0.4, SIZE > 10\n'
process += 'Number of sentence: ' + str(i) + '\n'
f.write(process)
f
print(len(tab))
print(i)


