#!usr/bin/python3

from wapiti import *

f = open("./sequoia-corpus.np_conll.v2", 'r')
text = f.read()
m = Model(patterns='*\nU:Wrd X=%x[0,0]')
m.add_training_sequence(text)
m.train()
m.save('sequoia-corpus-v6.0.save')
