#!usr/bin/python3

from wapiti import *

for i in range(0,11):
	print(i)
	f = open("frwikinews-20130110-pages-articles.txt.tok.stanford-pos."+str(i), 'r')
	text = f.read()
	if (i != 0):
		m = Model(patterns='*\nU:Wrd X=%x[0,0]', \
			model='free-french-treebankv2.save')
	else:
		m = Model(patterns='*\nU:Wrd X=%x[0,0]')
	m.add_training_sequence(text)
	m.train()
	m.save('free-french-treebankv2.save')
