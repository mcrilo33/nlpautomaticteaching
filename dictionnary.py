import re
import nltk.data
import sqlite3
import csv

dictionnaryPath = './Dictionnary/Lexique380.csv'
dbName = 'NLPAutomaticTeaching.db'

# Create table
conn = sqlite3.connect(dbName)
c = conn.cursor()
c.execute('''CREATE TABLE IF NOT EXISTS dictionnary \
		(word, phon, stem, pos, gender, number, movie_freq, \
		book_freq, verb_infos, homogr_nb, homoph_nb, islem, l_size, \
		phon_size, PRIMARY KEY(word, pos))''' \
	)

# Get infos from dictionnary
with open(dictionnaryPath, 'r', encoding='<latin1>') as csvfile:
	r = csv.reader(csvfile, delimiter='\t', quotechar='|')
	tab = []
	for row in r:
		tab.append([row[0],row[1],row[2],row[3],row[4],row[5], row[8], row[9], \
		row[10], row[11], row[12], row[13], row[14], row[15]])

c.executemany('INSERT OR IGNORE INTO dictionnary VALUES \
	(?,?,?,?,?,?,?,?,?,?,?,?,?,?)', tab
	)

conn.commit()
conn.close()
